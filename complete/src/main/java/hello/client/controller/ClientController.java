package hello.client.controller;

import hello.client.model.Client;
import hello.client.service.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/client")
@CrossOrigin(origins = "*")
public class ClientController {

    @Autowired
    private IClientService service;

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public Client create(@RequestBody Client client) {
        return service.create(client);

    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public Client update(@RequestBody Client client) {
        return service.update(client);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Client get(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Client> getAll() {
        return service.getAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        service.delete(id);
    }

}
