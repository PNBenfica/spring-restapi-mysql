package hello.client.service;

import hello.client.model.Client;

import java.util.List;

public interface IClientService {

    Client create(Client client);

    Client update(Client client);

    Client get(Long id);

    List<Client> getAll();

    void delete(Long id);

}
