package hello.client.service;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import hello.client.dao.IClientDao;
import hello.client.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements IClientService {

    @Autowired
    private IClientDao dao;

    public Client create(Client client) {
        return dao.create(client);
    }

    public Client update(Client client) {
        return dao.update(client);
    }

    public Client get(Long id) {
        return dao.get(id);
    }

    public List<Client> getAll() {
        return dao.getAll();
    }

    public void delete(Long id) {
        dao.delete(id);
    }

}
