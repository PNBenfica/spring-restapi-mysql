package hello.client.dao;

import hello.client.model.Client;
import hello.client.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Repository
public class ClientDaoImpl implements IClientDao {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Client create(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public Client update(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public Client get(Long id) {
        return clientRepository.findById(id).orElse(null);
    }

    @Override
    public List<Client> getAll() {
        Iterator<Client> iter = clientRepository.findAll().iterator();
        List<Client> clientsList = new ArrayList<>();
        while (iter.hasNext())
            clientsList.add(iter.next());

        return clientsList;
    }

    @Override
    public void delete(Long id) {
        clientRepository.deleteById(id);
    }

}

