package hello.client.dao;

import hello.client.model.Client;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public interface IClientDao {
    Client create(Client client);
    Client get(Long id);
    Client update(Client client);
    List<Client> getAll();
    void delete(Long id);
}
